<h2>Instalacia na PHP server</h2>
<ul>
    <li>
        Stiahnut vsetky subory ako zip
    </li>
    <li>
        Je nutne pouzit PHP hosting (min PHP 7) a MySQL databazou (nebude fungovat na free hostingu lebo nepodporuje CORS a https)
    </li>
    <li>
        Rozbalit zip a nahrat subory na server cez FTP
    </li>
    <li>
        Vytvorit MySQL databazu a naimportovat sql dump: <b>conf/db_dump.sql</b>
    </li>
    <li>
        Navigovat na https://moja-subdomena.moja-domena.com/configure a spusti sa prvotne nastavenie
    </li>
    <li>
        Vyplnit prihlasovacie udaje na prihlasenie do databazi
    </li>
    <li>
        Ak chces pouzivat Trakt, zaloz si apku a vypln aj <b>client_id</b> a <b>client_secret</b> (https://trakt.tv/oauth/applications/new)
    </li>
    <li>
        Po uspesnom vyplni udajov sa presmeruje na detailne nastavenia addonu s moznostou pridania do Stremia
    </li>
    <li>
        Tip: pouzi Stremio Addon Manager (https://addon-manager.dontwanttos.top/) na presunutie SCC addonu na prve miesto v Stremiu a budu sa zobrazovat meta informacie zo Stream Cinema aj pre tituly s IMDB id 
    </li>
</ul>

<h2>Licencia</h2>

<ul>
    <li>
        Ziaden z autorov tejto aplikacie nieje vlastnikom akehokolvek obsahu, ktory je zobrazovany v aplikacii. 
    </li>
    <li>
        Tato aplikacia sluzi len na studine ucely. 
    </li>
    <li>
        Tato aplikacia nema nic spolocne so zdrojom dat a udajov.
    </li>
    <li>
        Je to len aplikacia tretej strany a nikto z tvorcov nezodpoveda za jej obsah alebo pripadne pouzite.
    </li>
</ul>