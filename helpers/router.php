<?php

class Router {

    protected $page;
    private $allowed = array(
        'catalog', 'configure', 'manifest',
        'meta', 'oauth', 'stream', 'video'
    );

	public function __construct($page) {
		$this->page = $page;
	}

	public function route() {
	    $filename = 'pages/' . $this->page . '.php';

	    if (in_array($this->page, $this->allowed) && file_exists($filename)) {
	        return $filename;
	    } else {
	        page404();
	    }
	}
}