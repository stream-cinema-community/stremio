<?php

function cacheKey($type, $cache_id) {
    $cache_id = md5($cache_id);
    return implode('_', array(substr($cache_id, 0, 2), $type, $cache_id));
}

function getRequestParams() {
    $requestArgs = new stdClass();

    $params = array('code', 'configuration', 'id', 'page', 'type');
    foreach ($params as $param) {
        $requestArgs->$param = $_GET[$param];
    }

    if (isset($_GET['extra'])) {
        parse_str($_GET['extra'], $requestArgs->extra);
        $requestArgs->extra = (object)$requestArgs->extra;
    }

    return $requestArgs;
}

function setHeaders($cache = true) {
    global $settings;

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

    header('Content-Type: application/json; charset=utf-8');

    if ($cache) {
        $ts = gmdate("D, d M Y H:i:s", time() + $settings->cache_seconds) . " GMT";
        header('Expires: ' . $ts);
        header('Pragma: cache');
        header('Cache-Control: max-age=' . $settings->cache_seconds);
    }
}

function showContent($content, $cache = true) {
    setHeaders($cache);

    echo json_encode($content);
    die();
}

function redirect($url, $token = null) {
    $root = $token ? '/' . $token : '';
    header('Location: ' . $root . $url);
    die();
}

function page404($error = '404 Page Not Found') {
    pageError(null, $error);
}

function page403($error = '403 You are not authorised') {
    pageError('403 Forbidden', $error);
}

function pageError($code = '404 Not Found', $error = '404 Page Not Found') {
    header('HTTP/1.1 '.$code);

    (new FileHelper('log/production.log', false))->log($error);

    echo $error;
    die();
}
