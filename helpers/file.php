<?php

class FileHelper {

    protected $file;
    protected $json;

    public function __construct($file, $json = true) {
        $this->file = $file;
        $this->json = $json;
    }

    public function read($defaults = null, $as_array = false) {
        if (file_exists($this->file)) {
            $contents = file_get_contents($this->file);
            if ($this->json) $contents = json_decode($contents, $as_array);

            return $contents;

        } else if (!file_exists($this->file) && $defaults) {
            $this->write($defaults);

            return $as_array ? (array) $defaults : (object) $defaults;

        } else {
            page404('404 File not found.');
        }
    }

    public function write($contents, $flags = null) {
        if ($this->json) $contents = json_encode($contents);

        file_put_contents($this->file, $contents, $flags);
    }

    public function log($message) {
        $log = $message.' | '.date("Y-m-d H:i:s").' | '.$_SERVER['REMOTE_ADDR'].' | '.$_SERVER['QUERY_STRING'].PHP_EOL;
        $this->write($log, FILE_APPEND);
    }
}