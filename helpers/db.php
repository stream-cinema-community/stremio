<?php

class DBHelper {

    protected $connection;
    protected $query;
    protected $show_errors = TRUE;
    protected $query_closed = TRUE;
    public $query_count = 0;

    public function __construct($dbhost = 'localhost', $dbuser = 'root', $dbpass = '', $dbname = '', $charset = 'utf8') {
        $this->connection = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
        if ($this->connection->connect_error) {
            $this->error('Failed to connect to MySQL - ' . $this->connection->connect_error);
        }
        $this->connection->set_charset($charset);
    }

    public function query($query) {
        if (!$this->query_closed) {
            $this->query->close();
        }
        if ($this->query = $this->connection->prepare($query)) {
            if (func_num_args() > 1) {
                $x = func_get_args();
                $args = array_slice($x, 1);
                $types = '';
                $args_ref = array();
                foreach ($args as $k => &$arg) {
                    if (is_array($args[$k])) {
                        foreach ($args[$k] as $j => &$a) {
                            $types .= $this->_gettype($args[$k][$j]);
                            $args_ref[] = &$a;
                        }
                    } else {
                        $types .= $this->_gettype($args[$k]);
                        $args_ref[] = &$arg;
                    }
                }
                array_unshift($args_ref, $types);
                call_user_func_array(array($this->query, 'bind_param'), $args_ref);
            }
            $this->query->execute();
            if ($this->query->errno) {
                $this->error('Unable to process MySQL query (check your params) - ' . $this->query->error);
            }
            $this->query_closed = FALSE;
            $this->query_count++;
        } else {
            $this->error('Unable to prepare MySQL statement (check your syntax) - ' . $this->connection->error);
        }
        return $this;
    }


    public function fetchAll($callback = null) {
        $params = array();
        $row = array();
        $meta = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
        while ($this->query->fetch()) {
            $r = array();
            foreach ($row as $key => $val) {
                $r[$key] = $val;
            }
            if ($callback != null && is_callable($callback)) {
                $value = call_user_func($callback, $r);
                if ($value == 'break') break;
            } else {
                $result[] = $r;
            }
        }
        $this->query->close();
        $this->query_closed = TRUE;
        return $result;
    }

    public function fetchArray() {
        $params = array();
        $row = array();
        $meta = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array(array($this->query, 'bind_result'), $params);
        $result = array();
        while ($this->query->fetch()) {
            foreach ($row as $key => $val) {
                $result[$key] = $val;
            }
        }
        $this->query->close();
        $this->query_closed = TRUE;
        return $result;
    }

    public function close() {
        return $this->connection->close();
    }

    public function numRows() {
        $this->query->store_result();
        return $this->query->num_rows;
    }

    public function affectedRows() {
        return $this->query->affected_rows;
    }

    public function lastInsertID() {
        return $this->connection->insert_id;
    }

    public function error($error) {
        if ($this->show_errors) {
            exit($error);
        }
    }

    public function get_trakt_tokens($user_id) {
        $query = $this->query(
            'SELECT * FROM trakt WHERE id = ?', $user_id
        );
        return (object) $query->fetchArray();
    }

    public function save_trakt_tokens($id, $credentials) {
        $data = array($id);
        foreach(range(1,2) as $index) {
           $data[] = $credentials['trakt_access_token'];
           $data[] = $credentials['trakt_refresh_token'];
           $data[] = $credentials['trakt_expires'];
        }

        $query = '
            INSERT INTO trakt (id, trakt_access_token, trakt_refresh_token, trakt_expires)
            VALUES (?, ?, ?, ?)
            ON DUPLICATE KEY UPDATE
                trakt_access_token = ?,
                trakt_refresh_token = ?,
                trakt_expires = ?
        ';
        $this->query($query, $data);
    }

    public function get_services($stremio_id) {
        $query = $this->query(
            'SELECT sc_id, stremio_title, sc_title FROM services WHERE stremio_id = ?', $stremio_id
        );
        return (object) $query->fetchArray();
    }

    public function save_services($data) {
        $query = '
            INSERT INTO services (stremio_id, sc_id, stremio_title, sc_title, trakt_id, trakt_title, type)
            VALUES (?, ?, ?, ?, ?, ?, ?)
        ';
        $this->query($query, $data);
    }

    public function get_catalogs($user_id) {
        $query = $this->query(
            'SELECT settings FROM catalogs WHERE id = ?', $user_id
        );
        $result = $query->fetchArray();

        return $result['settings'] ? json_decode($result['settings'], true) : false;
    }

    public function save_catalogs($user_id, $settings) {
        $settings = json_encode($settings);
        $data = array($user_id, $settings, $settings);

        $query = '
            INSERT INTO catalogs (id, settings)
            VALUES (?, ?)
            ON DUPLICATE KEY UPDATE
                settings = ?
        ';
        $this->query($query, $data);
    }

    public function get_other_settings($user_id) {
        $settings['cache'] = 'full';
        $settings['posters'] = 'off';

        $db_settings = $this->get_catalogs($user_id);
        if ($db_settings) {
            $settings['cache'] = $db_settings['cache'] ?: $settings['cache'];
            $settings['posters'] = $db_settings['posters'] ?: $settings['posters'];
        }

        return $settings;
    }

    public function get_login_counter($username) {
        $query = $this->query(
            'SELECT attempts, timestamp FROM counter WHERE id = ?', md5($username)
        );
        $result = $query->fetchArray();

        return array(
            'attempts' => $result['attempts'] ?: 0,
            'timestamp' => $result['timestamp'] ?: time()
        );
    }

    public function save_login_counter($username, $attempts) {
        $data = array(md5($username), $attempts, $_SERVER['REMOTE_ADDR'], $attempts, $_SERVER['REMOTE_ADDR']);

        $query = '
            INSERT INTO counter (id, attempts, ip)
            VALUES (?, ?, ?)
            ON DUPLICATE KEY UPDATE
                attempts = ?,
                ip = ?
        ';
        $this->query($query, $data);
    }

	private function _gettype($var) {
	    if (is_string($var)) return 's';
	    if (is_float($var)) return 'd';
	    if (is_int($var)) return 'i';
	    return 'b';
	}
}
