<?php

class StreamHelper {

    protected $mbps_limit;
    protected $plugin_name;
    protected $entity_name;
    protected $crypt_helper;
    protected $configuration;

    public function __construct($settings) {
        global $crypt_helper;

        $this->mbps_limit = $settings['mbps_limit'];
        $this->plugin_name = $settings['plugin_name'];
        $this->filename = $settings['filename'];
        $this->configuration = $settings['configuration'];

        $this->crypt_helper = $crypt_helper;
    }

    public function process($streams) {
        $data = array();

        foreach ($streams as $stream) {
            $video = $stream->video[0];
            $resolution = $this->determine_resolution($video);
            $speed = $this->determine_speed($stream);

            if ($speed >= $this->mbps_limit) continue;

            $stream_info = new stdClass();
            $stream_info->name = $this->build_stream_name($video, $resolution, $stream->audio);
            $stream_info->description = $this->build_stream_description($stream);
            $stream_info->url = $this->build_stream_url($stream);
            $stream_info->quality = $resolution;
            $stream_info->speed = $speed;

            // $stream_info->video = $stream->video;
            $stream_info->audio = $stream->audio;
            // $stream_info->stream = $stream;

            $subtitles = $this->build_subtitles($stream);
            if ($subtitles && count($subtitles) > 0) {
                $stream_info->subtitles = $subtitles;
            }

            $stream_info->behaviorHints = array(
                'notWebReady' => true,
                'bingeGroup' => $this->build_stream_group($stream),
                'videoSize' => $stream->size,
                'filename' => $this->filename
            );

            $data[] = $stream_info;
        }

        array_multisort(
            array_column($data, 'quality'),  SORT_DESC,
            array_column($data, 'speed'), SORT_DESC,
            $data
        );

        return $data;
    }

    private function build_stream_name($video, $resolution, $audio) {
        if ($resolution < 720) {
            $name = array('SD');
        } else if ($resolution == 720) {
            $name = array('HD');
        } else if ($resolution == 1080) {
            $name = array('FHD');
        } else {
            $name = array('UHD');
        }

        $name[] = $resolution . 'p';

        $hdr = $video->hdr;
        if ($hdr) {
            if ((string)$hdr == 'Dolby Vision') {
                $hdr = 'DV';
            } else if (str_contains((string)$hdr, 'Dolby Vision')) {
                $hdr = 'DV + HDR';
            } else {
                $hdr = 'HDR';
            }
            $name[] = $hdr;
        }

        $languages = $this->sorted_languages($audio);
        $languages = array_intersect($languages, array('cz', 'sk'));
        if (count($languages) > 0) {
            $name[] = '(' . strtoupper(implode(',', $languages)) . ')';
        }

        return implode(" \n", $name);
    }

    private function build_stream_description($stream) {
        $description = array($this->plugin_name);

        $video = $stream->video[0];
        $bytes = $stream->size;
        $duration = $video->duration;

        if ($bytes >= 1073741824) {
            $size = number_format($bytes / 1073741824, 2) . ' GB';
        } else {
            $size = number_format($bytes / 1048576, 2) . ' MB';
        }

        $line1 = array('💾 ' . $size);

        if ($duration) {
            $line1[] = '(' . $this->determine_speed($stream) . ' Mb/s)';
        }

        $line2 = array("🎥 " . $video->codec);
        $line2[] = sprintf('(%02d:%02d:%02d)', $duration / 3600, floor($duration / 60) % 60, $duration % 60);

        $line3 = array();
        $languages = $this->sorted_languages($stream->audio);
        if ($languages && count($languages) > 0) {
            $line3[] = '🔊 ' . implode(',', array_map('strtolower', $languages));
        } else {
            $audio = $stream->audio[0];
            $line3[] = '🔊 ' . $this->build_audio_codec($audio);
        }

        $subtitles = $this->sorted_languages($stream->subtitles);
        if ($subtitles && count($subtitles) > 0) {
            $line3[] = '📑 ' . implode(',', array_map('strtolower', $subtitles));
        }

        $description[] = implode(' ‎  ', $line1);
        $description[] = implode(' ‎  ', $line2);
        $description[] = implode(' ‎  ', $line3);

        return implode(" \n", $description);
    }

    private function build_stream_url($stream) {
        $data = (array)$stream;

        $keys = array_keys($data);
        sort($keys);

        $base = $data[$keys[5]] . $data[$keys[3]];
        $pass = hash('sha256', utf8_encode($base));

        $data = array(
            'ident' => $stream->ident,
            'pass' => $pass
        );
        $code = $this->crypt_helper->encrypt($data);

        return "https://{$_SERVER['HTTP_HOST']}/" . $this->configuration . '/video/' .$code . '.strm';
    }

    private function build_stream_group($stream) {
        $group = array('streamcinema');
        $group[] = $this->determine_resolution($stream->video[0]) . 'p';

        $languages = array_unique(array_column($stream->audio, 'language'));
        $languages = array_intersect($languages, array('cs', 'sk', 'en'));
        if ($languages && count($languages) > 0) {
            $group[] = implode('-', $languages);
        }

        return implode('-', $group);
    }

    private function build_audio_codec($audio){
        $channels = array(
            2 => 'stereo',
            6 => '5.1 channel',
            8 => '7.1 surround'
        );
        return $audio->codec . ' (' . ($channels[$audio->channels] ?: $audio->channels.' channels') . ')';
    }

    private function build_subtitles($stream) {
        global $webshare, $cache, $other_settings;

        $subtitles_cache_key = cacheKey('subtitles', $stream->ident);

        if ($other_settings['cache'] == 'off') $cache->delete($subtitles_cache_key);

        $subtitles = $cache->get($subtitles_cache_key);
        if (!$subtitles) {
            $subtitles = array();

            foreach($stream->subtitles as $subtitle) {
                $lang = $this->get_language($subtitle->language);
                if (!$lang || !$subtitle->src) continue;

                $ident = end(explode('/', $subtitle->src));
                $src = $webshare->unprotected_file_link($ident);

                $subtitles[] = array(
                    'id' => $subtitle->_id,
                    'url' => $src ?: $subtitle->src,
                    'lang' => $lang
                );
            }

            if (!empty($subtitles)) {
                $cache->set($subtitles_cache_key, $subtitles, strtotime('+1 month'));
            }
        }

        return $subtitles;
    }

    private function determine_speed($stream) {
        $size = $stream->size;
        $duration = $stream->video[0]->duration;

        $speed = $size / floatval($duration) * 8 / 1048576;
        return floatval(number_format($speed, 2));
    }

    private function determine_resolution($video) {
        $width = $video->width;
        $height = $video->height;
        $aspect = $video->aspect;

        $widths = array(640, 848, 1280, 1920, 2560, 3840, 7680);
        $heights = array(144, 240, 360, 480, 720, 1080, 1440, 2160, 4320);

        $closest_by_width = $this->closest($width, $widths);
        $resolution_by_width = round($closest_by_width / (16 / 9), 0);

        $closest_on_height = $this->closest($resolution_by_width, $heights);
        $resolution_by_height = $this->closest($height, $heights);

        if ($resolution_by_height > 260 && $resolution_by_width != $resolution_by_height && $aspect > 2) {
            $resolution_by_height = $resolution_by_width;
        }

        return $resolution_by_height;
    }

    private function sorted_languages($data) {
        $languages = array_unique(array_column($data, 'language'));
        $languages = array_filter($languages, 'strlen');
        $languages = str_replace('cs', 'cz', $languages);
        sort($languages);

        return $languages;
    }

    private function closest($needle, $haystack) {
        foreach ($haystack as $item) {
            if ($closest === null || abs($needle - $closest) > abs($item - $needle)) {
                $closest = $item;
            }
        }

        return $closest;
    }

    private function get_language($lang) {
        $lang_map = array(
            'cs' => 'cze',
            'sk' => 'slk',
            'en' => 'eng',
        );

        return $lang_map[$lang];
    }
}
