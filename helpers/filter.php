<?php

class FilterHelper {

    const GENRES = array(
        'Action', 'Animated', 'Animation', 'Adventure', 'Biographical', 'Biography', 'Comedy', 'Crime',
        'Documentary', 'Drama', 'Fairy Tale', 'Family', 'Fantasy', 'History', 'Horror',
        'Musical', 'Mysterious', 'Psychological', 'Romance', 'Sci-Fi', 'Superhero', 'Sports',
        'Thriller', 'Travel', 'War', 'Western'
    );

    public $customized = false;

    private $filters;

    public function __construct($filters) {
        $this->filters = $filters;

        $this->process();
        $this->sort();
    }

    public function get_filters($all = false) {
        if (!$all) {
            return array_filter($this->filters, function($filter) {
                return !!$filter['active'];
            });
        }

        return $this->filters;
    }

    public function get_filter($id) {
        $filter = $this->filters[$id];
        return !!$filter['active'] ? $filter : false;
    }

    private function process() {
        global $db, $user_id;

        $catalogs = $db->get_catalogs($user_id);
        if ($catalogs) {
            $this->customized = true;

            foreach($this->filters as &$filter) {
                $id = $filter['id'];
                if (!$id || $filter['locked']) continue;

                $filter['active'] = !!$catalogs['active'][$id];
                $filter['order'] = $catalogs['order'][$id];
                $filter['sort'] = array_search($id, $catalogs['sort']);
                $filter['catalog']['name'] = $catalogs['name'][$id];

                if ($filter['custom']) {
                    $filter['params']['value']['user'] = $catalogs['user'][$id];
                    $filter['params']['value']['list'] = $catalogs['list'][$id];

                    $filter['params']['value']['items'] = array();
                    if (!!$catalogs['movie'][$id]) $filter['params']['value']['items'][] = 'movie';
                    if (!!$catalogs['show'][$id])  $filter['params']['value']['items'][] = 'show';
                }
            }
        }
    }

    private function sort() {
        uasort($this->filters, function($a, $b) {
            return $a['sort'] <=> $b['sort'];
        });
    }
}