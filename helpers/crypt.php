<?php

class CryptHelper {

    protected $cipher = "AES-128-CBC";

    private $passphrase;
    private $salt;

    public function __construct($secrets) {
        $this->passphrase = $secrets->passphrase;
        $this->salt = $secrets->salt;
    }

    public function encrypt($data) {
        $json = json_encode($data);
        $encrypted = openssl_encrypt(
            $json,
            $this->cipher,
            $this->passphrase,
            0,
            $this->salt
        );

        return $this->base64_encode_url($encrypted);
    }

    public function decrypt($ciphertext) {
        $decoded = $this->base64_decode_url($ciphertext);
        $json = openssl_decrypt(
            $decoded,
            $this->cipher,
            $this->passphrase,
            0,
            $this->salt
        );

        return json_decode($json);
    }

    private function base64_encode_url($string) {
        return str_replace(['+','/','='], ['-','_',''], base64_encode($string));
    }

    private function base64_decode_url($string) {
        return base64_decode(str_replace(['-','_'], ['+','/'], $string));
    }
}