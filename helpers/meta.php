<?php

class MetaHelper {

    protected $media;
    protected $type;
    protected $imdb_id;
    protected $background;

    public function __construct($media, $imdb_id = null, $background = null) {
        $this->media = $media;

        $source = $this->media->_source ?: $this->media;
        $i18n_info = $source->i18n_info_labels;

        $this->imdb_id = $imdb_id ?: $source->services->imdb;
        $this->background = $background ?: $this->get_image($i18n_info, 'background');
    }

    public function transform() {
        $source = $this->media->_source ?: $this->media;
        $info = $source->info_labels;
        $i18n_info = $source->i18n_info_labels;
        $services = $source->services;

        $media = new stdClass();
        // $media->source = $source;

        $media->sc_id = 'sc' . $this->media->_id;
        $media->id = $this->imdb_id ?: $media->sc_id;
        $media->name = $this->get_first($i18n_info, 'title') ?: $this->get_en($i18n_info, 'title');
        $media->description = $this->get_first($i18n_info, 'plot');

        $prefix = [];
        if ($languages = $source->available_streams->languages->audio->map) {
            $languages = str_replace('cs', 'cz', $languages);
            $languages = array_intersect($languages, array('cz', 'sk'));
            sort($languages);
            if (count($languages) > 0) {
                $prefix[] = 'Audio: ' . strtoupper(implode(', ', $languages)) ;
            }
        }
        if ($csfd = $source->ratings->csfd->rating) {
            $prefix[] = 'CSFD: ' . $csfd * 10 . '%';
        }

        if (count($prefix) > 0) {
            $media->description = '(' . implode(', ', $prefix) . ') ' . $media->description;
        }

        $media->background = $this->background;
        $media->poster = $this->get_image($i18n_info, 'poster');
        $media->logo = $this->get_image($i18n_info, 'logo');

        $media->imdb_id = $this->imdb_id;
        $media->trakt_id = $services->trakt;
        $media->trakt_with_type = $services->trakt_with_type;
        $media->moviedb_id = (int) $services->tmdb;
        $media->slug = $this->build_slug($source);

        $media->imdbRating = (string) $source->ratings->overall->rating;

        $cast = array_column($source->cast, 'name');
        $media->cast = array_slice($cast, 0, 5);

        $media->director = array_slice($info->director, 0, 5);
        $media->writer = array_slice($info->writer, 0, 5);

        $genres = array_intersect($info->genre, FilterHelper::GENRES);
        $media->genres = array_slice($genres, 0, 5);
        $media->genre = $media->genres;

        $media->year = (string) $info->year;
        $media->status = $info->status;
        $media->type = $info->mediatype == 'movie' ? 'movie' : 'series';
        $this->type = $media->type;
        $media->dateadded = $info->dateadded;

        $premiered = $info->premiered ?: $info->aired;
        if ($premiered) {
            $date = new DateTime($premiered);
            $media->released = $date->format('Y-m-d\TH:i:s.u\Z');
        }

        if ($info->year) $media->releaseInfo = (string) $info->year;
        if ($info->duration) $media->runtime = round($info->duration / 60, 1) . ' min';

        $trailer = $source->videos[0];
        if ($trailer) {
            $video_id = $this->youtube_id($trailer->url);
            $media->trailers = array(array(
                'source' => $video_id,
                'type' => 'Trailer'
            ));
        }

        if ($media->type != 'series') {
            $media->behaviorHints = (object)array('defaultVideoId' => $media->id);
        }

        $media->links = $this->generate_links($media->genres, $media->cast, $media->director, $media->imdbRating);

        return $media;
    }

    public function get_seasons($children_list, $sc_id) {
        $seasons = array();
        foreach($children_list as $child) {
            $source = $child->_source;
            $info = $source->info_labels;

            if ($info->mediatype == 'season' && $info->season != 0 && $source->available_streams->count > 0) {
                $seasons[] = array(
                    'sc_id' => $child->_id,
                    'season' => $info->season
                );
            }

        }
        if (empty($seasons)) {
            $seasons[] = array(
                'sc_id' => $sc_id,
                'season' => 1
            );
        }

        return $seasons;
    }

    public function get_episodes($children_list) {
        $episodes = array();

        foreach($children_list as $child) {
            $source = $child->_source;
            $info = $source->info_labels;

            if ($info->mediatype == 'episode' && $source->available_streams->count > 0) {
                $episodes[] = $this->transform_episode($child->_id, $source);
            }
        }

        return $episodes;
    }

    public function decideExpires($datestring, $type) {
        global $other_settings;

        if (!$datestring) return strtotime('+1 hour');

        $datediff = time() - strtotime($datestring);
        $daysdiff = $datediff / (60 * 60 * 24);

        $additions = array(
            'meta' => [
                15 => '+1 day',
                30 => '+5 days',
                90 => '+2 weeks',
                360 => '+2 months',
                720 => '+4 months',
            ],
            'meta_half' => [
                15 => '+12 hours',
                30 => '+3 days',
                90 => '+1 weeks',
                360 => '+1 months',
                720 => '+2 months',
            ],
            'episode' => [
                15 => '+1 day',
                30 => '+7 days',
                90 => '+1 month',
                360 => '+3 months'
            ],
            'episode_half' => [
                15 => '+12 hours',
                30 => '+4 days',
                90 => '+2 weeks',
                360 => '+2 months'
            ]
        );

        if ($other_settings['cache'] == 'half') {
            $type = $type . '_half';
            $defaultAddition = '+6 months';
        } else {
            $defaultAddition = '+1 year';
        }

        foreach ($additions[$type] as $threshold => $value) {
            if ($daysdiff < $threshold) {
                $addition = $value;
                break;
            }
        }

        // If $addition is not set (i.e., $daysdiff >= 720), assign default value
        $addition = $addition ?? $defaultAddition;

        return strtotime($addition);
    }

    private function transform_episode($id, $episode) {
        $info = $episode->info_labels;
        $i18n_info = $episode->i18n_info_labels;

        $premiered = $info->premiered ?: $info->aired;
        $date = new DateTime($premiered ?: $info->dateadded);

        $media = new stdClass();
        // $media->id = 'sc' . $id;
        $media->id = $this->imdb_id ? implode(':', [$this->imdb_id, $info->season, $info->episode]) :  'sc' . $id;
        $media->available = true;

        $media->released = $date->format('Y-m-d\TH:i:s.u\Z');
        $media->dateadded = $info->dateadded;

        $media->season = $info->season;
        $media->episode = $info->episode;

        $media->title = $this->get_first($i18n_info, 'title') ?: ($info->originaltitle ?: 'Episode ' . $media->episode);
        $media->overview = $this->get_first($i18n_info, 'plot');
        $media->description = $media->overview;

        if ($this->imdb_id) {
            $image_id = implode('/', [$this->imdb_id, $media->season, $media->episode]);
        }
        $media->thumbnail = $this->get_image($i18n_info, 'thumbnail', $image_id);

        return $media;
    }

    private function generate_links($genres, $casts, $directors, $rating) {
        global $manifest;

        $links = array();
        if ($rating) {
            $links[] = array(
                'name' => $rating,
                'category' => 'imdb',
                'url' => 'https://imdb.com/title/' . $this->imdb_id
            );
        }

        $base = str_replace('{type}', $this->type, '/{type}/{type}_popular');

        if ($genres && count($genres) > 0) {
            foreach($genres as $genre) {
                $links[] = array(
                    'name' => $genre,
                    'category' => 'Genres',
                    'url' => 'stremio:///discover/' . urlencode($manifest) . $base . '?genre=' . $genre
                );
            }
        }

        if ($casts && count($casts) > 0) {
            foreach($casts as $cast) {
                $links[] = array(
                    'name' => $cast,
                    'category' => 'Cast',
                    'url' => 'stremio:///search?search=' . urlencode($cast)
                );
            }
        }

        if ($directors && count($directors) > 0) {
            foreach($directors as $director) {
                $links[] = array(
                    'name' => $director,
                    'category' => 'Directors',
                    'url' => 'stremio:///search?search=' . urlencode($director)
                );
            }
        }

        return $links;
    }

    private function get_image($source, $type, $image_id = null) {
        global $db;

        $query = $db->query('SELECT poster, background, logo FROM images WHERE sc_id = ?', 'sc' . $this->media->_id);
        $images_override = $query->fetchArray();

        switch ($type) {
            case 'poster':
                $image = $this->get_first($source, 'art', 'poster');
                if ($image) $image = str_replace('t/p/original', 't/p/w300', $image);
                if ($this->imdb_id) $fallback = 'https://images.metahub.space/poster/small/' . $this->imdb_id . '/img';
                if (!$image && !$fallback) $image = $this->get_image($source, 'background');
                break;

            case 'background':
                $image = $this->get_first($source, 'art', 'fanart');
                if ($image) $image = str_replace('t/p/original', 't/p/w1280', $image);
                if ($this->imdb_id) $fallback = 'https://images.metahub.space/background/medium/' . $this->imdb_id . '/img';
                break;

            case 'thumbnail':
                if ($image_id) $fallback = 'https://episodes.metahub.space/'. $image_id . '/w780.jpg';
                $image = $this->get_first($source, 'art', 'fanart');
                if ($image) {
                    $image = str_replace('t/p/original', 't/p/w500', $image);

                    if ($this->background) {
                        $image_end = end(explode('/', $image));
                        $background_end = end(explode('/', $this->background));

                        if ($image_end == $background_end && $image_id) $image = null;
                    }
                }
                break;

            case 'logo':
                if ($this->imdb_id) $image = 'https://images.metahub.space/logo/medium/' . $this->imdb_id . '/img';
                $fallback = $this->get_first($source, 'art', 'clearlogo');
                break;
        }

        $image = $image ?: $fallback;
        if ($images_override[$type]) $image = $images_override[$type];

        $image = str_replace('http://', 'https://', $image);

        //fix images without prefix
        if (str_starts_with($image, '//') && !str_contains($image, 'http')) {
            $image = 'https:' . $image;
        }
        return $image;
    }

    private function get_first($haystack, $level1, $level2 = null) {
        $value = null;
        foreach($haystack as $object) {
            $data = $object->$level1;
            if ($level2 && $data) {
                $data = $data->$level2 ?: null;
            }

            if ($data) {
                $value = $data;
                break;
            }
        }

        return $value;
    }

    private function get_en($haystack, $needle) {
        $value = null;
        foreach($haystack as $object) {
            if ($object->lang == 'en') {
                $value = $object->$needle;
                break;
            }
        }

        return $value;
    }

    private function youtube_id($url) {
        $video_id = explode("?v=", $url);
        if (empty($video_id[1])) $video_id = explode("/v/", $url);

        $video_id = explode("&", $video_id[1]);
        $video_id = $video_id[0];

        return $video_id;
    }

    private function build_slug($source) {
        $type = $source->info_labels->mediatype == 'movie' ? 'movie' : 'series';
        $title = str_replace(' ', '-', preg_replace('/[^A-z0-9 ]/', '', $this->get_en($source->i18n_info_labels, 'title')));
        return $type . '/' . strtolower($title) . str_replace('tt', '-', $this->imdb_id);
    }
}
