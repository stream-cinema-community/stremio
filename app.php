<?php

error_reporting(E_ERROR | E_PARSE);

session_start();

require 'vendor/autoload.php';

include 'helpers/base.php';
include 'helpers/crypt.php';
include 'helpers/db.php';
include 'helpers/file.php';
include 'helpers/filter.php';
include 'helpers/meta.php';
include 'helpers/router.php';
include 'helpers/stream.php';

include 'services/cinemeta.php';
include 'services/stream_cinema.php';
include 'services/trakt.php';
include 'services/webshare.php';

$conf = 'conf/conf.json';
if (!file_exists($conf)) {
    include 'pages/install.php';

} else {
    $settings = (new FileHelper($conf))->read();
    $device = $settings->device;

    // get request parameters
    $params = getRequestParams();

    // initialize database from config
    $db = new DBHelper(
        $settings->db->dbhost,
        $settings->db->dbuser,
        $settings->db->dbpass,
        $settings->db->dbname
    );

    // initialize caching library
    $cache = new \rapidweb\RWFileCache\RWFileCache();
    $cache->changeConfig(array(
        'cacheDirectory' => 'cache/'
    ));

    // initialize crypting library
    $crypt_helper = new CryptHelper($settings->crypt);

    // initialize cinemeta library
    $cinemeta = new Cinemeta($settings->api->cinemeta);

    // initialize stream cinema library
    $stream_cinema = new StreamCinema(
        $settings->api->stream_cinema,
        $settings->stream_cinema_token,
        $settings->endpoints->stream_cinema
    );

    // initialize webshare library
    $encrypted = $params->configuration ?: $_SESSION['configuration_token'];
    $webshare_credentials = $crypt_helper->decrypt($encrypted);
    $webshare = new Webshare($settings->api->webshare, array(
        'username' => $webshare_credentials->username,
        'salt' => $webshare_credentials->salt,
        'password' => $webshare_credentials->password
    ));

    $user_id = md5($webshare_credentials->username.$webshare_credentials->salt.$device->uuid);

    $trakt_credentials = $db->get_trakt_tokens($user_id);
    $trakt = new Trakt($settings->api->trakt, $trakt_credentials);

    $filters = (new FileHelper('conf/filters.json'))->read(null, true);
    $filter_helper = new FilterHelper($filters);

    $other_settings = $db->get_other_settings($user_id);

    $manifest = "https://{$_SERVER['HTTP_HOST']}/{$params->configuration}/manifest.json";

    $router = new Router($params->page);
    include $router->route();
}
