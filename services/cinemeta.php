<?php

class Cinemeta {

    protected $current_api;
    protected $live_api;

    public function __construct($settings) {
        $this->current_api = $settings->current;
        $this->live_api = $settings->live;
    }

    public function movie_id($id) {
        $response = $this->request('movie', $id);
        $meta = $response->meta;

        if ($meta) {
            $tmdb_id = $meta->moviedb_id;
            $tvdb_id = $meta->tvdb_id;
            $name = implode(', ' , [$meta->name, $meta->releaseInfo]);
        }

        return [$tmdb_id, $tvdb_id, $name];
    }

    public function series_id($episode_id, $id) {
        $response = $this->request('series', $id);
        $meta = $response->meta;

        if ($meta) {
            $videos = $meta->videos;

            if ($videos && count($videos) > 0) {
                list($tmdb_id, $tvdb_id, $name) =
                    $this->get_by_index(
                        $videos, $episode_id, 'id', 'name'
                    );
            }

            $name_info = implode(', ' , [$meta->name, $meta->releaseInfo, $name]);
        }

        if (!$tmdb_id || !$tvdb_id) {
            $response = $this->request('series', $id, true);
            $meta = $response->meta;

            if ($meta) {
                $videos = $meta->videos;

                if ($videos && count($videos) > 0) {
                    list($tmdb_id, $tvdb_id, $title) =
                        $this->get_by_index(
                            $videos, $episode_id, 'id', 'title', $tmdb_id, $tvdb_id
                        );

                    if ($name && (!$tmdb_id || !$tvdb_id)) {
                        list($tmdb_id, $tvdb_id, $title) =
                            $this->get_by_index(
                                $videos, $name, 'title', 'title', $tmdb_id, $tvdb_id
                            );
                    }
                }

                if (!$name_info) {
                    $name_info = implode(', ' , [$meta->name, $meta->releaseInfo, $title]);
                }
            }
        }

        return [$tmdb_id, $tvdb_id, $name_info];
    }

    private function request($type, $id, $live = false) {
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );

        $api = trim(($live ? $this->live_api : $this->current_api), '/');
        $url = implode('/', [$api, $type, $id]) . '.json';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($ch);
        $result = json_decode($response);

        curl_close($ch); // close the connection

        return $result;
    }

    private function get_by_index(
        $haystack,
        $needle,
        $column,
        $name_key,
        $tmdb_id = null,
        $tvdb_id = null
    ) {
        $keys = array_column($haystack, $column);

        if ($column == 'title') {
            $needle = $this->unify_titles($needle);
            $keys = array_map(array($this, 'unify_titles'), $keys);
        }

        $index = array_search($needle, $keys);

        if ($index !== false) {
            $found = $haystack[$index];

            if (!$tmdb_id) $tmdb_id = $found->moviedb_id;
            if (!$tvdb_id) $tvdb_id = $found->tvdb_id;
            $name = $found->$name_key;
        }

        return [$tmdb_id, $tvdb_id, $name];
    }

    private function unify_titles($value) {
        $to_remove = array(
            'the, ',
            'the ', ' the ', ' the',
            'a ', ' a ', ' a'
        );
        $value = strtolower($value);
        $value = str_replace($to_remove, '', $value);
        return preg_replace("/[^A-Za-z0-9]/", '', $value);
    }
}
