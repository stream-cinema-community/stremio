<?php

class Webshare {

    protected $api;
    protected $username;
    protected $salt;
    protected $password;

    public function __construct($api, $credentials) {
        $this->api = $api;

        $this->username = $credentials['username'];
        $this->salt = $credentials['salt'];
        $this->password = $credentials['password'];
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function configured() {
        return $this->username && $this->salt && $this->password;
    }

    public function username_salt() {
        $params = array(
            'username_or_email' => $this->username
        );

        $result = $this->request('salt', $params);
        $salt = (string)$result->salt;

        if ($this->valid($result) && $salt) {
            $this->salt = $salt;
            return $salt;
        } else {
            return null;
        }
    }

    public function file_link($ident, $pass) {
        $params = array(
            'ident' => $ident,
            'password' => $this->file_pass($ident, $pass),
            'download_type' => 'video_stream',
            'device_vendor' => 'Stremio',
            'device_model' => 'Stremio addon'
        );

        $result = $this->request('file_link', $params, true);
        $url = (string)$result->link;

        if ($this->valid($result) && $url) {
            return $url;
        } else {
            return null;
        }
    }

    public function unprotected_file_link($ident) {
        $params = array(
            'ident' => $ident,
            'device_vendor' => 'Stremio',
            'device_model' => 'Stremio addon'
        );

        $result = $this->request('file_link', $params, true);
        $url = (string)$result->link;

        if ($this->valid($result) && $url) {
            return $url;
        } else {
            return null;
        }
    }

    public function vip() {
        if (!$_SESSION['webshare_vip']) {
            $result = $this->request('user_data', array(), true);
            $vip = (boolean)$result->vip;
            $vip_days = (integer)$result->vip_days;

            if ($this->valid($result) && $vip) {
                $_SESSION['webshare_vip'] = true;
                $_SESSION['webshare_vip_days'] = $vip_days;
            } else {
                $_SESSION['webshare_vip_days'] = 0;
                $_SESSION['webshare_vip'] = null;
            }
        }

        return [$_SESSION['webshare_vip'], $_SESSION['webshare_vip_days']];
    }

    public function logout() {
        $_SESSION['webshare_wst'] = null;
        $_SESSION['webshare_vip'] = null;
        $_SESSION['webshare_vip_days'] = 0;
    }

    private function request($endpoint, $params = array(), $use_token = false) {
        global $device;

        $headers = array(
            'Accept: text/xml; charset=UTF-8',
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
            'X-Uuid: ' . $device->uuid
        );

        $api = trim($this->api, '/');
        $url = implode('/', [$api, $endpoint]) . '/';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);

        // use token is requested
        if ($use_token) $params['wst'] = $this->get_token();

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

        // receive server response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($ch);
        $result = simplexml_load_string($response);

        curl_close($ch); // close the connection

        return $result;
    }

    private function file_pass($ident, $pass) {
        $params = array(
            'ident' => $ident
        );

        $result = $this->request('file_password_salt', $params, true);
        $salt = (string)$result->salt;

        if ($this->valid($result) && $salt) {
            $crypted_password = crypt($pass, '$1$' . $salt);
            $hashed_password = sha1($crypted_password, false);

            return $hashed_password;
        } else {
            return null;
        }
    }

    private function set_token() {
        $crypted_password = crypt($this->password, '$1$' . $this->salt);
        $hashed_password = sha1($crypted_password, false);

        $params = array(
            'username_or_email' => $this->username,
            'password' => $hashed_password,
            'keep_logged_in' => 1
        );

        $result = $this->request('login', $params);
        $token = (string)$result->token;

        if ($this->valid($result) && $token) {
            $_SESSION['webshare_wst'] = $token;
        } else {
            $_SESSION['webshare_wst'] = null;
        }
    }

    private function get_token() {
        if (!$_SESSION['webshare_wst']) {
            $this->set_token();
        }

        return $_SESSION['webshare_wst'];
    }

    private function valid($result) {
        return (string)$result->status == 'OK';
    }
}
