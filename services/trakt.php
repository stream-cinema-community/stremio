<?php

class Trakt {

    const API_VERSION = 2;

    protected $provider;
    protected $api;
    protected $client_id;
    protected $access_token;
    protected $refresh_token;
    protected $expires;

    public function __construct($settings, $credentials = null) {
        $this->api = $settings->api;
        $this->client_id = $settings->client_id;

        $this->provider = new \League\OAuth2\Client\Provider\GenericProvider(array(
            'clientId'       => $this->client_id,
            'clientSecret'   => $settings->client_secret,
            'redirectUri'    => 'https://' . $_SERVER['HTTP_HOST'] . '/trakt-oauth',
            'urlAuthorize'   => $this->api . $settings->authorize,
            'urlAccessToken' => $this->api . $settings->access_token,
            'urlResourceOwnerDetails' => $this->api . $settings->resource_owner
        ));

        if ($credentials) {
            $this->access_token = $credentials->trakt_access_token;
            $this->refresh_token = $credentials->trakt_refresh_token;
            $this->expires = $credentials->trakt_expires;

            if ($this->expired() && $this->refresh_token) {
                $this->refreshAccessToken($this->refresh_token);
            }
        }
    }

    public function getAuthorizationUrl() {
        return $this->provider->getAuthorizationUrl();
    }

    public function getState() {
        return $this->provider->getState();
    }

    public function getAccessToken($code) {
        $params = array(
            'code' => $code
        );
        return $this->provider->getAccessToken('authorization_code', $params);
    }

    public function refreshAccessToken($refresh_token) {
        global $db, $user_id;

        $params = array(
            'refresh_token' => $refresh_token
        );
        $newAccessToken = $this->provider->getAccessToken('refresh_token', $params);

        $this->access_token = $newAccessToken->getToken();
        $this->refresh_token = $newAccessToken->getRefreshToken();
        $this->expires = $newAccessToken->getExpires();

        // Store access tokens
        $db->save_trakt_tokens($user_id, array(
            'trakt_access_token' => $this->access_token,
            'trakt_refresh_token' => $this->refresh_token,
            'trakt_expires' => $this->expires
        ));
    }

    public function logout() {
        global $db, $user_id;

        $db->save_trakt_tokens($user_id, array(
            'trakt_access_token' => null,
            'trakt_refresh_token' => null,
            'trakt_expires' => null
        ));
    }

    public function connected() {
        return $this->access_token && $this->refresh_token && !$this->expired();
    }

    public function movie_id($imdb_id) {
        $movie = $this->apiRequest('movies/' . $imdb_id);

        if ($movie) {
            $name = implode(', ' , array($movie['title'], $movie['year']));
            return [$movie['ids']['trakt'], $name];
        }
    }

    public function episode_id($imdb_id, $season, $episode) {
        $episode = $this->apiRequest('shows/' . $imdb_id . '/seasons/' . $season . '/episodes/' . $episode);

        if ($episode) {
            return [$episode['ids']['trakt'], $episode['title']];
        }
    }

    public function show_id($imdb_id) {
        $show = $this->apiRequest('shows/' . $imdb_id);
        if ($show) {
            return [$show['ids']['trakt'], $show['title']];
        }
    }

    public function show_info($trakt_id) {
        $show = $this->apiRequest('shows/' . $trakt_id, array('extended' => 'full'));

        if ($show) return $show;
    }

    public function apiRequest($endpoint, $params = array()){
        if ($this->access_token) {
            $request = $this->provider->getAuthenticatedRequest(
                'GET',
                $this->api . $endpoint . '?' . http_build_query($params),
                $this->access_token,
                array(
                    'headers' => array(
                        'Content-type' => 'application/json',
                        'trakt-api-key' => $this->client_id,
                        'trakt-api-version' => self::API_VERSION
                    )
                )
            );

            return $this->provider->getParsedResponse($request);
        }
    }

    private function expired() {
        return ($this->expires - time()) <= 0;
    }
}