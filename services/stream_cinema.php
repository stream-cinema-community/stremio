<?php

class StreamCinema {

    const LIMIT = 100;

    protected $api;
    protected $endpoints;

    private $token;

    public function __construct($api, $token, $endpoints) {
        $this->api = $api;
        $this->token = $token;
        $this->endpoints = $endpoints;
    }

    public function episode_id($id, $season, $episode, $service = 'imdb') {
        list($parent_id, $name) = $this->entity_id($id, $service);

        return $this->numbering_id($parent_id, $season, $episode);
    }

    public function entity_id($id, $service = 'imdb', $type = '*') {
        $params = array(
            'type' => $type,
            'service' => $service,
            'value' => $id
        );

        $result = $this->request($this->endpoints->info, $params);

        $hits = $result->hits->hits[0];
        $info = $hits->_source->info_labels;

        $name = implode(', ' , [$info->originaltitle, $info->year]);

        return [$hits->_id, $name];
    }

    public function media_filter($filter, $params, $from = 0, $force_limit = false) {
        $endpoint = str_replace('{filter}', $filter, $this->endpoints->filter);

        if ($from !== false) {
            $params['from'] = $from;
            $params['size'] = self::LIMIT;
        }
        if ($force_limit !== false) $params['size'] = $force_limit;

        $result = $this->request($endpoint, $params);

        return $result;
    }

    public function media_info($sc_id) {
        $endpoint = str_replace('{media_id}', $sc_id, $this->endpoints->media);
        $result = $this->request($endpoint);

        return $result;
    }

    public function media_children($parent_id) {
        $params = array(
            'value' => $parent_id,
            'sort' => 'episode'
        );
        $response = $this->media_filter('parent', $params, false, 1000);
        $children_list = $response->hits->hits;

        return ($children_list && count($children_list) > 0) ? $children_list : [];
    }

    public function streams($sc_id) {
        $endpoint = str_replace('{media_id}', $sc_id, $this->endpoints->streams);
        $result = $this->request($endpoint);

        return $result;
    }

    private function numbering_id($parent_id, $season, $episode) {
        $params = array(
            'root_parent' => $parent_id,
            'season' => $season,
            'episode' => $episode,
        );

        $result = $this->request($this->endpoints->episode, $params);

        $hit = $result->hits->hits[0];

        return [$hit->_id, $hit->_source->info_labels->originaltitle];
    }

    private function request($endpoint, $params = array()) {
        global $device;

        $headers = array(
            'User-Agent: Kodi/20.2 (Macintosh; Intel Mac OS X 13_6_6) App_Bitness/64 Version/20.2-(20.2.0)-Git:20230629-5f418d0b13',
            'X-Uuid: ' . $device->uuid,
        );
        $params['access_token'] = $this->token;

        $api = trim($this->api, '/');
        $url = implode('/', [$api, $endpoint]) . '?' . $this->encodeParams($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        // Debugging: Enable verbose mode and capture verbose output
        // $verbose = fopen('php://temp', 'w+');  // Use a temporary stream to capture output
        // curl_setopt($ch, CURLOPT_VERBOSE, true);
        // curl_setopt($ch, CURLOPT_STDERR, $verbose);

        $response = curl_exec($ch);
        // Debugging: Get the verbose output (request and response debug information)
        // rewind($verbose);
        // $verboseOutput = stream_get_contents($verbose);

        // Close the cURL session
        curl_close($ch);

        // Debugging: Show the request and response
        // echo "Verbose Output:\n" . $verboseOutput . "\n\n";
        // echo "Response:\n" . $response . "\n";

        return json_decode($response);
    }

    private function encodeParams($data){
        $params = [];
        foreach ($data as $name => $val) {
            if (is_array($val)) {
                foreach ($val as $v) {
                    $params[] = sprintf('%s=%s', rawurldecode($name), rawurlencode($v));
                }
            } else {
                $params[] = sprintf('%s=%s', rawurldecode($name), rawurlencode($val));
            }
        }
        return implode('&', $params);
    }
}
