-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: db.websupport.sk:3306
-- Generation Time: May 14, 2024 at 11:00 AM
-- Server version: 10.5.22-MariaDB-1:10.5.22+maria~ubu2004-log
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stremio_addon`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalogs`
--

CREATE TABLE `catalogs` (
  `id` varchar(50) NOT NULL,
  `settings` varchar(1200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `counter`
--

CREATE TABLE `counter` (
  `id` varchar(50) NOT NULL,
  `attempts` tinyint(4) NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `sc_id` varchar(50) NOT NULL,
  `poster` varchar(255) DEFAULT NULL,
  `background` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`sc_id`, `poster`, `background`, `logo`) VALUES
('sc5ed07e024cb589554fefbf7e', 'https://m.media-amazon.com/images/M/MV5BNGM4YWY2NjUtMzA2OC00ZWMwLWFjMDctYjlhZTQyM2FlMGNiXkEyXkFqcGdeQXVyMzU0NzkwMDg@._V1_FMjpg_UY1123_.jpg', NULL, NULL),
('sc6162bebb7ab1c810df0a2756', NULL, NULL, 'https://images.metahub.space/logo/small/tt0106004/img');

-- --------------------------------------------------------

--
-- Table structure for table `seasons`
--

CREATE TABLE `seasons` (
  `sc_id` varchar(50) NOT NULL,
  `parent_id` varchar(50) NOT NULL,
  `season` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `stremio_id` varchar(50) NOT NULL,
  `sc_id` varchar(50) NOT NULL,
  `stremio_title` varchar(255) DEFAULT NULL,
  `sc_title` varchar(255) DEFAULT NULL,
  `trakt_id` varchar(50) DEFAULT NULL,
  `trakt_title` varchar(255) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trakt`
--

CREATE TABLE `trakt` (
  `id` varchar(50) NOT NULL,
  `trakt_access_token` text DEFAULT NULL,
  `trakt_refresh_token` text DEFAULT NULL,
  `trakt_expires` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catalogs`
--
ALTER TABLE `catalogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`sc_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `season` (`season`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`stremio_id`),
  ADD UNIQUE KEY `stremio_id_to_sc_id_unique` (`stremio_id`,`sc_id`),
  ADD KEY `sc_id_index` (`sc_id`),
  ADD KEY `trakt_id_index` (`trakt_id`);

--
-- Indexes for table `trakt`
--
ALTER TABLE `trakt`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
