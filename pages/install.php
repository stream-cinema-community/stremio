<?php

$dbhost = $_POST['dbhost'];
$dbname = $_POST['dbname'];
$dbusername = $_POST['dbusername'];
$dbpassword = $_POST['dbpassword'];

$clientid = $_POST['clientid'];
$clientsecret = $_POST['clientsecret'];

$mbsplimit = $_POST['mbsplimit'];

if ($dbhost && $dbname && $dbusername && $dbpassword) {
    try {
        $db = new DBHelper($dbhost, $dbusername, $dbpassword, $dbname);
    } catch (Exception $e) {
        $db_error = $e->getMessage();
    }

    if (!$db_error) {
        $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4));
        $passphrase = bin2hex(openssl_random_pseudo_bytes(16));
        $salt = bin2hex(openssl_random_pseudo_bytes(16));

        $default_settings = (new FileHelper('conf/conf.json.default'))->read();

        $default_settings->db->dbhost = $dbhost ?: 'localhost';
        $default_settings->db->dbname = $dbname;
        $default_settings->db->dbuser = $dbusername;
        $default_settings->db->dbpass = $dbpassword;
        $default_settings->api->trakt->client_id = $clientid;
        $default_settings->api->trakt->client_secret = $clientsecret;
        $default_settings->limit_mbps = $mbsplimit ?: 50;
        $default_settings->device->uuid = $uuid;
        $default_settings->crypt->passphrase = $passphrase;
        $default_settings->crypt->salt = $salt;

        (new FileHelper('conf/conf.json'))->write($default_settings);

        $_SESSION['initial_setup_success'] = true;
        redirect('/configure');
    }
}

?>

<!DOCTYPE html>
<html lang='en'>
    <head>
        <title>Initial Setup</title>
        <meta charset='utf-8'>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
    </head>
    <body>
        <form method="post">
            <div class="px-4 mt-5 text-center">
                <h2 class="display-6 fw-bold text-body-emphasis">Initial Setup</h1>

                <div class="col-lg-6 mx-auto">
                    <p class="lead mb-4">
                        Setup database, max stream speed limit. and Trakt.tv api <a href="https://trakt.tv/oauth/applications/new" target="_blank">https://trakt.tv/oauth/applications/new</a>
                    </p>

                    <?php if ($db_error) : ?>
                        <div class="alert alert-danger d-flex align-items-center col-sm-10 mx-auto" role="alert">
                            <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                            </svg>
                            <div>
                                <?= $db_error ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <h4 class="fw-bold text-body-emphasis">Database</h4>

                    <div class="mb-2 row">
                        <label for="dbhost" class="offset-2 col-sm-2 col-form-label text-start">DB Host</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dbhost" name="dbhost" value="<?= $dbhost ?: 'localhost' ?>" />
                        </div>
                    </div>
                    <div class="mb-2 row">
                        <label for="dbname" class="offset-2 col-sm-2 col-form-label text-start">DB Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dbname" name="dbname" value="<?= $dbname ?>" />
                        </div>
                    </div>
                    <div class="mb-2 row">
                        <label for="dbusername" class="offset-2 col-sm-2 col-form-label text-start">DB User</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dbusername" name="dbusername" value="<?= $dbusername ?>" />
                        </div>
                    </div>
                    <div class="mb-2 row">
                        <label for="dbpassword" class="offset-2 col-sm-2 col-form-label text-start">DB Password</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="dbpassword" name="dbpassword" value="<?= $dbpassword ?>" />
                        </div>
                    </div>
                    <h4 class="fw-bold text-body-emphasis">Trakt.tv</h4>
                    <div class="mb-2 row">
                        <label for="clientid" class="offset-2 col-sm-2 col-form-label text-start">Client ID</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="clientid" name="clientid" value="<?= $clientid ?>" />
                        </div>
                    </div>
                    <div class="mb-2 row">
                        <label for="clientsecret" class="offset-2 col-sm-2 col-form-label text-start">Client Secret</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="clientsecret" name="clientsecret" value="<?= $clientsecret ?>" />
                        </div>
                    </div>

                    <h4 class="fw-bold text-body-emphasis">Other</h4>
                    <div class="mb-2 row">
                        <label for="mbsplimit" class="offset-2 col-sm-2 col-form-label text-start">Mbps Limit</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="mbsplimit" name="mbsplimit" value="<?= $mbsplimit ?: 50 ?>" />
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary btn-lg px-4 mt-4">
                    Save Changes
                </button>
            </div>
        </form>
        <br />
        <br />
    </body>
</html>
