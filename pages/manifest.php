<?php

$filters = $filter_helper->get_filters();

// declare manifest
$manifest = new stdClass();
$manifest->id = "com.stremio.streamcinema.addon";
$manifest->version = "1.0.0";
$manifest->name = "Stream Cinema";
$manifest->description = "Used to serve streams from Stream Cinema.";
$manifest->logo = "https://{$_SERVER['HTTP_HOST']}/logo.jpg";
$manifest->background = "https://{$_SERVER['HTTP_HOST']}/background.png";

$stream = new stdClass();
$stream->name = "stream";
$stream->types = array("movie", "series");
$stream->idPrefixes = array("tt", "sc");

$meta = new stdClass();
$meta->name = "meta";
$meta->types = array("movie", "series");
$meta->idPrefixes = array("tt", "sc");

$manifest->resources = array('catalog', 'subtitles', (array)$meta, (array)$stream);
$manifest->types = array("movie", "series", "anime", "other");

$behaviorHints = new stdClass();
$behaviorHints->configurable = true;
$behaviorHints->configurationRequired = !$webshare->configured();

$manifest->behaviorHints = (array)$behaviorHints;

// set catalogs in manifest
$manifest->catalogs = array();

foreach($filters as $id => $filter) {
    $catalog = $filter['catalog'];
    $catalog['id'] = $id;

    foreach($catalog['extra'] as &$extra) {
        if ($extra['options'] == 'genres') {
            $extra['options'] = FilterHelper::GENRES;
        } else if ($extra['options'] == 'years') {
            $extra['options'] = array_map('strval', range(date("Y"), 1985));
        }
    }

    $manifest->catalogs[] = $catalog;
}

// print manifest in JSON format
showContent((array)$manifest, false);
