<?php

list($vip, $vip_days) = $webshare->vip();
if (!$vip) {
    page403('403 Webshare VIP expired');
}

$encrypted = $params->code;
$decrypted = $crypt_helper->decrypt($encrypted);

if ($decrypted->ident && $decrypted->pass) {
    $url = $webshare->file_link($decrypted->ident, $decrypted->pass);
}

if ($url) {
    redirect($url);

} else {
    page404('404 Stream Link Not Found');
}
