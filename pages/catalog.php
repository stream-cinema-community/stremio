<?php

$metas = array();
$filter = $filter_helper->get_filter($params->id);

if ($filter) {
    $skip = $params->extra->skip ?: 0;
    $genre = $params->extra->genre;

    if (!$filter['locked']) {
        $catalog_cache_key = cacheKey('catalog', $filter['id'] . $skip . $genre);

        if ($other_settings['cache'] == 'off') {
            $cache->delete($catalog_cache_key);
        }

        $metas = $cache->get($catalog_cache_key);
    }

    if (!$metas) {
        // trakt playlists
        if ($filter['params']['service'] == 'trakt') {
            $value = $filter['params']['value'];

            if ($filter['custom'] && is_array($value)) {
                $url = 'users/{u}/lists/{l}/items/{i}';

                $search = array('{u}','{l}', '{i}');
                $replace = array($value['user'], $value['list'], implode(',', $value['items']));

                $value = str_replace($search, $replace, $url);
            }

            // items from trakt cant be sorted by api
            $list_items = $trakt->apiRequest($value);

            // sort by date desc
            if ($filter['order'] == 'date') {
                usort($list_items, function($a, $b) {
                    return strtotime($b['listed_at']) - strtotime($a['listed_at']);
                });
            } else if ($filter['order'] == 'random') {
                usort($list_items, function() {
                    return rand(-1, 1);
                });
            }

            // pagination with skip parameter
            $list_items = array_slice($list_items, $skip, StreamCinema::LIMIT);

            // transform items for stream cinema
            if ($list_items) {
                $imdb_map = array();
                $trakt_ids = array_map(
                    function($item) use (&$imdb_map) {
                        $type = $item['type'];
                        $key = ($type == 'show' ? 'tv' : '') . $type;
                        $ids = $item[$type]['ids'];

                        $imdb_map[$ids['trakt']] = $ids['imdb'];

                        return implode(':', array($key, $ids['trakt']));
                    },
                    $list_items
                );
            }

            $filter['params'] = array(
                'type' => '*',
                'service' => 'trakt_with_type',
                'value' => $trakt_ids
            );
            $skip = 0;
        }

        // fulltext search filters
        $search = $params->extra->search;
        if ($filter['filter'] == 'search' && $search) {
            $filter['params']['value'] = $search;
        }

        // genre and year filter
        if ($genre) {
            if ($filter['params']['sort'] == 'dateAdded') {
                $filter['filter'] = 'year';
            } else {
                $filter['filter'] = 'genre';
            }
            $filter['params']['value'] = $genre;
        }

        $response = $stream_cinema->media_filter($filter['filter'], $filter['params'], $skip);

        $hits = $response->hits->hits;
        if ($hits && count($hits) > 0) {
            $persist = array();
            $unique = array();

            foreach($hits as $hit) {
                $source = $hit->_source;
                $genres = $source->info_labels->genre ?: [];
                $tags = $source->tags ?: [];

                // skip adult
                $adult = array('Erotic', 'Pornographic');
                if (!empty(array_intersect($adult, $genres)) || in_array('porno', $tags)) {
                    continue;
                }

                $trakt_id = $source->services->trakt;
                $imdb_id = $imdb_map[$trakt_id];

                // transform data
                $meta_helper = new MetaHelper($hit, $imdb_id);
                $meta = $meta_helper->transform();

                $metas[] = $meta;

                if (str_starts_with($meta->id, 'tt')) {
                    if (in_array($meta->id, $unique)) continue;
                    $unique[] = $meta->id;

                    $persist[] = array(
                         'stremio_id' => $meta->id,
                         'sc_id' => $hit->_id,
                         'stremio_title' => $hit->_source->info_labels->originaltitle,
                         'sc_title' => $meta->name,
                         'trakt_id' => $meta->trakt_id,
                         'type' => $meta->type
                    );
                }
            }

            if (!$filter['locked'] && count($metas) > 0) {
                $cache_time = $other_settings['cache'] == 'half' ? '+12 hours' : '+24 hours';
                $cache->set($catalog_cache_key, $metas, strtotime($cache_time));
            }

            if (count($persist) > 0) {
                $ids = array_column($persist, 'stremio_id');
                $wildcards = implode(',', array_fill(0, count($ids), '?'));

                $query = $db->query('SELECT stremio_id FROM services WHERE stremio_id IN (' . $wildcards . ')', $ids);
                $existing = $query->fetchAll();
                $existing = array_column($existing, 'stremio_id');

                $persist = array_filter($persist, function($item) use ($existing) {
                    return !in_array($item['stremio_id'], $existing);
                });

                if (count($persist) > 0) {
                    $data = array();
                    $insert = array('INSERT INTO services (stremio_id, sc_id, stremio_title, sc_title, trakt_id, type) VALUES');
                    foreach($persist as $item) {
                        $data = array_merge($data,
                            array(
                                $item['stremio_id'], $item['sc_id'], $item['stremio_title'],
                                $item['sc_title'], $item['trakt_id'], $item['type']
                            )
                        );
                    }
                    $insert[] = implode(',', array_fill(0, count($persist), '(?,?,?,?,?,?)'));
                    $db->query(implode(' ', $insert), $data);
                }
            }
        }
    }
}

showContent(array('metas' => $metas));
