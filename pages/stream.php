<?php

$media_id = $params->id;
$search_type = $params->type;

if (str_starts_with($media_id, 'sc')) {
    $sc_id = substr($media_id, 2);

    $media_info = $stream_cinema->media_info($sc_id);
    $sc_name = $media_info->info_labels->originaltitle;

} else {
    $services = $db->get_services($media_id);
    if ($services->sc_id) {
        $sc_id = $services->sc_id;
        $sc_name = $services->stremio_title ?: $services->sc_title;

    } else {
        list($imdb_id, $season, $episode) = explode(':', $media_id);

        if ($search_type == 'movie') {
            list($sc_id, $sc_name) = $stream_cinema->entity_id($imdb_id);

            if (!$sc_id) {
                list($trakt_id, $trakt_title) = $trakt->movie_id($imdb_id);

                if ($trakt_id) {
                    list($sc_id, $sc_name) = $stream_cinema->entity_id($trakt_id, 'trakt', 'movie');
                }

                if (!$sc_id) {
                    list($tmdb_id, $tvdb_id, $cinemeta_name) = $cinemeta->movie_id($imdb_id);

                    if ($tmdb_id) {
                        list($sc_id, $sc_name) = $stream_cinema->entity_id($tmdb_id, 'tmdb');
                    }
                    if (!$sc_id && $tvdb_id) {
                        list($sc_id, $sc_name) = $stream_cinema->entity_id($tvdb_id, 'tvdb');
                    }
                }
            }

        } else if ($search_type == 'series') {
            list($sc_id, $sc_name) = $stream_cinema->episode_id($imdb_id, $season, $episode);

            if (!$sc_id) {
                list($trakt_id, $trakt_title) = $trakt->episode_id($imdb_id, $season, $episode);

                if ($trakt_id) {
                    list($sc_id, $sc_name) = $stream_cinema->entity_id($trakt_id, 'trakt', 'episode');
                }
            }

            if (!$sc_id) {
                list($tmdb_id, $tvdb_id, $cinemeta_name) = $cinemeta->series_id($media_id, $imdb_id);

                if ($tmdb_id) {
                    list($sc_id, $sc_name) = $stream_cinema->entity_id($tmdb_id, 'tmdb');
                }
                if (!$sc_id && $tvdb_id) {
                    list($sc_id, $sc_name) = $stream_cinema->entity_id($tvdb_id, 'tvdb');
                }
            }

            if (!$sc_id) {
                list($trakt_show_id) = $trakt->show_id($imdb_id);

                if ($trakt_show_id) {
                    list($sc_id, $sc_name) = $stream_cinema->episode_id($trakt_show_id, $season, $episode, 'trakt');
                }
            }

        } else {
            page404();
        }

        if ($sc_id) {
            $data = array($media_id, $sc_id, $cinemeta_name, $sc_name, $trakt_id, $trakt_title, $search_type);
            $db->save_services($data);
        }
    }
}

if ($sc_id) {
    $streams = $stream_cinema->streams($sc_id);

    if ($streams && count($streams) > 0) {
        $stream_helper = new StreamHelper(array(
            'mbps_limit' => $settings->limit_mbps,
            'plugin_name' => $settings->plugin_name,
            'filename' => $cinemeta_name ?: $sc_name,
            'configuration' => $params->configuration
        ));

        $processed = $stream_helper->process($streams);

        // print streams in JSON format
        showContent(array('streams' => $processed));

    } else {
        page404();
    }

} else {
    page404();
}
