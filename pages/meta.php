<?php

$media_id = $params->id;
$search_type = $params->type;

if (str_starts_with($media_id, 'sc') || str_starts_with($media_id, 'tt')) {

    // serve movies by cinemeta or sc id
    if (str_starts_with($media_id, 'tt')) {
        $services = $db->get_services($media_id);
        $sc_id = $services->sc_id;
        $imdb_id = $media_id;

        if (!$sc_id) {
            list($sc_id, $sc_name) = $stream_cinema->entity_id($media_id);

            if ($sc_id) {
                $data = array($media_id, $sc_id, null, $sc_name, null, null, $search_type);
                $db->save_services($data);
            }
        }
    } else {
        $sc_id = substr($media_id, 2);
    }

    $meta_cache_key = cacheKey('meta', $sc_id);
    $latest_cache_key = cacheKey('latest', $sc_id);
    $older_cache_key = cacheKey('older', $sc_id);

    if ($other_settings['cache'] == 'off') {
        $cache->delete($meta_cache_key);
        $cache->delete($latest_cache_key);
        $cache->delete($older_cache_key);
    }

    $meta = $cache->get($meta_cache_key);
    if (!$meta) {
        $media_info = $stream_cinema->media_info($sc_id);

        if ($media_info) {
            $meta_helper = new MetaHelper($media_info, $imdb_id);
            $meta = $meta_helper->transform();

            if ($meta->type == 'series' && $meta->trakt_id) {
                $show = $trakt->show_info($meta->trakt_id);
                if ($show) {
                    $meta->status = $show['status'];
                    $meta->ended = in_array($show['status'], array('canceled', 'ended'));
                }
            }

            $expires = $meta_helper->decideExpires($meta->released, 'meta');
            $cache->set($meta_cache_key, $meta, $expires);
        }
    }

    if ($meta->type == 'series') {
        $meta_helper = new MetaHelper($meta, $meta->imdb_id, $meta->background);

        $query = $db->query('SELECT sc_id, updated_at FROM seasons WHERE parent_id = ? ORDER BY season', $sc_id);
        $seasons = $query->fetchAll();

        $expired_seasons = true;
        if (!empty($seasons)) {
            $cache_reduction = $other_settings['cache'] == 'half' ? '-2 months' : '-4 months';
            $reduction = $meta->ended ? $cache_reduction : '-1 week';

            $diff = strtotime($seasons[0]['updated_at']) - strtotime($reduction);
            if ($diff > 0) $expired_seasons = false;
        }

        if ($expired_seasons) {
            $stored_seasons = count($seasons);
            $season_list = $stream_cinema->media_children($sc_id);
            $seasons = $meta_helper->get_seasons($season_list, $sc_id);

            $data = array();
            $insert = array('INSERT INTO seasons (sc_id, parent_id, season) VALUES');
            foreach($seasons as $season) {
                $data = array_merge($data, array($season['sc_id'], $sc_id, $season['season']));
            }
            $insert[] = implode(',', array_fill(0, count($seasons), '(?, ?, ?)'));
            $insert[] ='ON DUPLICATE KEY UPDATE updated_at = current_timestamp()';

            $db->query(implode(' ', $insert), $data);

            if ($stored_seasons != count($seasons)) {
                $cache->delete($latest_cache_key);
                $cache->delete($older_cache_key);
            }
        }

        $latest_season = array_pop($seasons);

        $latest_episodes = $cache->get($latest_cache_key);
        if (!$latest_episodes) {
            $episode_list = $stream_cinema->media_children($latest_season['sc_id']);
            $latest_episodes = $meta_helper->get_episodes($episode_list);

            $latest = end($latest_episodes);
            $cache_time = $other_settings['cache'] == 'half' ? '+3 months' : '+6 months';
            $expires =
                $meta->ended ?
                    strtotime($cache_time) :
                    $meta_helper->decideExpires($latest->released, 'episode');

            $cache->set($latest_cache_key, $latest_episodes, $expires);
        }

        if (!empty($seasons)) {
            $older_episodes = $cache->get($older_cache_key);

            if (!$older_episodes) {
                $older_episodes = array();

                foreach($seasons as $season) {
                    $episode_list = $stream_cinema->media_children($season['sc_id']);
                    $episodes = $meta_helper->get_episodes($episode_list);

                    $older_episodes = array_merge($older_episodes, $episodes);
                }

                $cache_time = $other_settings['cache'] == 'half' ? '+6 months' : '+1 year';
                $cache->set($older_cache_key, $older_episodes, strtotime($cache_time));
            }
        }

        if ($older_episodes) {
            $meta->videos = array_merge($older_episodes, $latest_episodes);
        } else {
            $meta->videos = $latest_episodes;
        }
    }

    // print meta in JSON format
    showContent(array('meta' => $meta));

} else {
    page404();
}
