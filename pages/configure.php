<?php

$filters = $filter_helper->get_filters(true);

$username = $_POST['username'];
$password = $_POST['password'];

if (isset($_POST['logout'])) {
    $webshare->logout();
    unset($_SESSION['configuration_token']);
    redirect('/configure');
}

if (isset($_POST['logout_trakt'])) {
    $trakt->logout();
    redirect('/configure', $_SESSION['configuration_token']);
}

if (isset($_POST['reset_filters'])) {
    $db->save_catalogs($user_id, null);
    redirect('/configure', $_SESSION['configuration_token']);
}

if ($username) {
    $yesterday = time() + 86400;
    $login_counter = $db->get_login_counter($username);

    if ($login_counter['attempts'] >= 5) {
        if ($yesterday < strtotime($login_counter['timestamp'])) {
            page403('403 Too many attempts. Try again tomorrow.');
        } else {
            $login_counter['attempts'] = 0;
        }
    }
}

if (!$webshare->configured() && $username && $password) {
    $webshare->setUsername($username);
    $webshare->setPassword($password);

    $webshare->username_salt();
    $webshare->logout();

    $webshare_submitted = true;
    $wrong_credentials = true;
}

if ($webshare->configured()) {
    list($vip, $vip_days) = $webshare->vip();

    if ($vip && $vip_days > 0) {
        $data = array(
            'username' => $webshare->getUsername(),
            'salt' => $webshare->getSalt(),
            'password' => $webshare->getPassword()
        );
        $token = $crypt_helper->encrypt($data);
        $_SESSION['configuration_token'] = $token;

        $login_success = true;
        $wrong_credentials = false;

        $db->save_login_counter($user_id, 0);

        if ($webshare_submitted) {
            $_SESSION['webshare_login_success'] = true;
            redirect('/configure', $_SESSION['configuration_token']);
        }

        if ($_POST['type'] == 'settings') {
            $settings = array(
                'active' => $_POST['active'],
                'sort' => $_POST['sort'],
                'name' => $_POST['name'],
                'user' => $_POST['user'],
                'list' => $_POST['list'],
                'movie' => $_POST['movie'],
                'show' => $_POST['show'],
                'order' => $_POST['order'],
                'cache' => $_POST['cache'],
                'posters' => $_POST['posters'],
            );
            $db->save_catalogs($user_id, $settings);

            $_SESSION['config_success'] = true;
            redirect('/configure', $_SESSION['configuration_token']);
        }
    }
}

if ($wrong_credentials && $username) {
    $db->save_login_counter($username, $login_counter['attempts'] + 1);
}

$webshare_login_success = $_SESSION['webshare_login_success'];
unset($_SESSION['webshare_login_success']);

$trakt_login_success = $_SESSION['trakt_login_success'];
unset($_SESSION['trakt_login_success']);

$config_success = $_SESSION['config_success'];
unset($_SESSION['config_success']);

$initial_setup_success = $_SESSION['initial_setup_success'];
unset($_SESSION['initial_setup_success']);

?>

<!DOCTYPE html>
<html lang='en'>
    <head>
        <title>Configure Stream Cinema Addon</title>
        <meta charset='utf-8'>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>

        <style type="text/css">
            .btn-success {
                background-color: #21b365;
                border-color: #21b365;
            }

            .btn-success:hover {
                background-color: #1ca35b;
                border-color: #1ca35b;
            }

            .drop-shadow {
                -webkit-filter: drop-shadow(2px 1px 0px #053b54);
                filter: drop-shadow(2px 1px 0px #053b54);
            }

            .catalog-handle {
                cursor: pointer;
            }

            .blue-background-class {
                background-color: #C8EBFB;
            }

            .w-90 {
                width: 90%;
            }

            .font-small {
                font-size: 13px
            }
        </style>
    </head>
    <body>
        <?php if ($webshare_login_success) : ?>
            <div class="alert alert-success d-flex align-items-center col-sm-4 mx-auto mt-3" role="alert">
                <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-check-circle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Success:">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </svg>
                <div>
                    Successfully logged in! You can install addon now.
                </div>
            </div>
        <?php endif; ?>

        <?php if ($trakt_login_success) : ?>
            <div class="alert alert-success d-flex align-items-center col-sm-4 mx-auto mt-3" role="alert">
                <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-check-circle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Success:">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </svg>
                <div>
                    Successfully logged in! You can configure trakt lists now.
                </div>
            </div>
        <?php endif; ?>

        <?php if ($initial_setup_success) : ?>
            <div class="alert alert-success d-flex align-items-center col-sm-4 mx-auto mt-3" role="alert">
                <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-check-circle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Success:">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </svg>
                <div>
                    Successfully completed initial setup! You can configure addon.
                </div>
            </div>
        <?php endif; ?>

        <?php if ($config_success) : ?>
            <div class="alert alert-success d-flex align-items-center col-sm-4 mx-auto mt-3" role="alert">
                <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-check-circle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Success:">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                </svg>
                <div>
                    Successfully saved configuration.
                </div>
            </div>
        <?php endif; ?>

        <form id="webshareLoginForm" method="post" class="d-flex">
            <div class="px-4 my-4 col-lg-5 col-sm-6 offset-lg-1 text-center <?= $login_success ? '' : 'mx-auto' ?>">
                <img class="d-block mx-auto mb-3 drop-shadow" src="/vip.png" alt="" height="75">
                <h2 class="display-6 fw-bold text-body-emphasis">Webshare settings</h1>

                <?php if ($login_success) : ?>
                    <div class="alert alert-info d-flex align-items-center mx-auto" role="alert">
                      <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-check-circle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Success:">
                          <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                      </svg>
                      <div class="d-flex align-items-center w-100">
                            You are webshare VIP member. <?= $vip_days ?> days remaining.
                            <button type="submit" name="logout" class="btn btn-sm btn-danger ms-auto">Logout</button>
                      </div>
                    </div>
                <?php endif; ?>

                <div class="mx-auto">
                    <?php if (!$login_success) : ?>
                        <p class="lead mb-4">Use your webshare login credentials to get access token for API.</p>
                    <?php endif; ?>

                    <?php if ($webshare_submitted && $wrong_credentials) : ?>
                        <div class="alert alert-danger d-flex align-items-center col-sm-10 mx-auto" role="alert">
                            <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                            </svg>
                            <div>
                                Access denied! Wrong username or password.
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!$login_success) : ?>
                        <div class="mb-2 row">
                            <label for="username" class="offset-2 col-sm-2 col-form-label text-start">Username</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="username" name="username" value="<?= $username ?>" />
                            </div>
                          </div>
                        <div class="mb-2 row">
                            <label for="inputPassword" class="offset-2 col-sm-2 col-form-label text-start">Password</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" id="inputPassword" name="password" />
                            </div>
                        </div>
                        <br />
                    <?php endif; ?>

                    <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <?php if (!$login_success) : ?>
                            <button id="loginBtn" type="submit" name="login" class="btn btn-primary btn-lg px-4 gap-3">
                                Login
                            </button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php if ($login_success) : ?>
                <div class="px-4 my-4 col-lg-5 col-sm-6 text-center">
                    <img class="d-block mx-auto mb-3" src="/trakt-icon.png" alt="" height="75">
                    <h2 class="display-6 fw-bold text-body-emphasis">Trakt settings</h1>

                    <div class="mx-auto">
                        <?php if ($trakt->connected()) : ?>
                            <div class="alert alert-info d-flex align-items-center" role="alert">
                                <svg style="width: 24px" xmlns="http://www.w3.org/2000/svg" class="bi bi-check-circle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Success:">
                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                                </svg>
                                <div class="d-flex align-items-center w-100">
                                    You are logged in to Trakt.tv.
                                    <button type="submit" name="logout_trakt" class="btn btn-sm btn-danger ms-auto">Logout</button>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="mx-auto">
                                <p class="lead my-4">
                                    Login to Trakt.tv to get your watchlist and thematic lists. <br /><br />
                                    <button id="traktLoginBtn" type="button" name="login" class="btn btn-primary btn-lg px-4 gap-3">
                                        Login
                                    </button>
                                </p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </form>


        <form id="addonSettingsForm" method="post">
            <input type="hidden" name="type" value="settings" />

            <div class="px-4 mt-5 text-center">
                <img class="d-block mx-auto mb-3" src="/logo.jpg" alt="" width="100">
                <h2 class="display-6 fw-bold text-body-emphasis">Addon settings</h1>

                <div class="col-lg-6 mx-auto">
                    <p class="lead mb-4">Configure stream cinema or trakt catalogs. Drag and drop to reorder.</p>

                    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                      <symbol id="move-icon" fill="currentColor" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5m0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5m0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5"/>
                      </symbol>
                    </svg>

                    <div class="d-flex px-3">
                        <span class="col-sm-1">Show</span>
                        <span class="col-sm-4">Name</span>
                        <span class="col-sm-3">User / List</span>
                        <span class="col-sm-2">Type</span>
                        <span class="col-sm-1">Order</span>
                        <span class="col-sm-1">Pos</span>
                    </div>

                    <ul class="list-group" id="catalogList">
                        <?php foreach($filters as $filter) : ?>
                            <?php if ($filter['locked']) continue; ?>
                            <?php if (!$trakt->connected() && $filter['trakt']) continue; ?>
                            <?php $id = $filter['id']; ?>

                            <li class="list-group-item d-flex align-items-center">
                                <span class="col-sm-1">
                                    <input type="checkbox" name="active[<?= $id ?>]" <?= $filter['active'] ? 'checked' : '' ?> />
                                    <input type="hidden" name="sort[]" value="<?= $id ?>" />
                                </span>
                                <span class="col-sm-4">
                                    <input class="w-90 font-small" type="text" name="name[<?= $id ?>]" value="<?= $filter['catalog']['name'] ?>" />
                                </span>
                                <span class="col-sm-3 font-small">
                                    <?php if ($filter['custom']) : ?>
                                        <input class="w-90 font-small" type="text" name="user[<?= $id ?>]" value="<?= $filter['params']['value']['user'] ?>" /> <br />
                                        <input class="w-90 font-small" type="text" name="list[<?= $id ?>]" value="<?= $filter['params']['value']['list'] ?>" />
                                    <?php else : ?>
                                        <?= $filter['info'] ?>
                                    <?php endif; ?>
                                </span>
                                <span class="col-sm-2">
                                    <?php if ($filter['custom']) : ?>
                                        <input id="movie-<?= $id ?>" type="checkbox" name="movie[<?= $id ?>]" <?= in_array('movie', $filter['params']['value']['items']) ? 'checked' : '' ?> />
                                        <label for="movie-<?= $id ?>">Movie</label>
                                        <br />
                                        <input id="show-<?= $id ?>" type="checkbox" name="show[<?= $id ?>]" <?= in_array('show', $filter['params']['value']['items']) ? 'checked' : '' ?> />
                                        <label for="show-<?= $id ?>">Series</label>
                                    <?php else : ?>
                                        <?= ucfirst($filter['catalog']['type']) ?>
                                    <?php endif; ?>
                                </span>
                                <span class="col-sm-1">
                                    <?php if ($filter['trakt']) : ?>
                                        <span class="text-nowrap">
                                            <input id="rank-<?= $id ?>" type="radio" value="rank" name="order[<?= $id ?>]" <?= $filter['order'] == 'rank' ? 'checked' : '' ?>  />
                                            <label for="rank-<?= $id ?>">Rank</label>
                                        </span>
                                        <br />
                                        <span class="text-nowrap">
                                            <input id="date-<?= $id ?>" type="radio" value="date" name="order[<?= $id ?>]" <?= $filter['order'] == 'date' ? 'checked' : '' ?>  />
                                            <label for="date-<?= $id ?>">Date</label>
                                        </span>
                                        <span class="text-nowrap">
                                            <input id="random-<?= $id ?>" type="radio" value="random" name="order[<?= $id ?>]" <?= $filter['order'] == 'random' ? 'checked' : '' ?>  />
                                            <label for="random-<?= $id ?>">Rand</label>
                                        </span>
                                    <?php endif; ?>
                                </span>
                                <span class="col-sm-1">
                                    <svg class="catalog-handle" width="16" height="16" role="img" aria-label="move:"><use xlink:href="#move-icon"/></svg>
                                </span>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="row mt-4 col-sm-6 mx-auto">
                        <label class="col-sm-4 col-form-label pt-1" for="cache">Cache:</label>
                        <div class="col-sm-8">
                            <select class="form-select form-select-sm" id="cache" name="cache">
                                <option value="off" <?= $other_settings['cache'] == 'off' ? 'selected' : '' ?>>Off</option>
                                <option value="half" <?= $other_settings['cache'] == 'half' ? 'selected' : '' ?>>Half</option>
                                <option value="full" <?= $other_settings['cache'] == 'full' ? 'selected' : '' ?>>Full</option>
                            </select>
                        </div>
                    </div>
                    <div class="row col-sm-6 mx-auto">
                        <label class="col-sm-4 col-form-label pt-1" for="posters">Info Posters:</label>
                        <div class="col-sm-8">
                            <select class="form-select form-select-sm" id="posters" name="posters">
                                <option value="off" <?= $other_settings['posters'] == 'off' ? 'selected' : '' ?>>Inactive</option>
                                <option value="on" <?= $other_settings['posters'] == 'on' ? 'selected' : '' ?>>Active</option>
                            </select>
                        </div>
                    </div>
                </div>



                <?php if ($filter_helper->customized) : ?>
                    <button type="submit" name="reset_filters" class="btn btn-link btn-sm px-4 mt-4" onclick="return confirm('Really?')">
                        Reset settings
                    </button>
                <?php endif; ?>

                <button type="submit" class="btn btn-primary btn-lg px-4 mt-4" <?= $login_success ? '' : 'disabled' ?> >
                    Save Changes
                </button>

                <button id="installLinkEnd" type="button" class="btn btn-success btn-lg px-4 mt-4" <?= $login_success ? '' : 'disabled' ?> >
                    <img src="/stremio-icon.png" height="30" style="vertical-align: bottom" /> Install Addon
                </button>
            </div>
        </form>
        <br />
        <br />
        <br />
        <br />

        <script type="text/javascript">
            Sortable.create(catalogList, {
                animation: 150,
                handle: '.catalog-handle',
                ghostClass: 'blue-background-class'
            });

            function installAddon() {
                const url = 'stremio://<?= $_SERVER['HTTP_HOST']; ?>/<?= $token ?>/manifest.json';
                navigator.clipboard?.writeText(url.replace('stremio://', 'https://'));
                window.location.href = url;
            }

            document.getElementById('installLink')?.addEventListener('click', installAddon);
            document.getElementById('installLinkEnd')?.addEventListener('click', installAddon);

            document.getElementById('traktLoginBtn')?.addEventListener('click', function(e) {
                e.preventDefault();
                const url = '/trakt-oauth';
                window.location.href = url;
            });

            document.getElementById('loginBtn')?.addEventListener('click', function() {
                this.disabled = true;
                this.innerHTML = `<span class="spinner-border spinner-border-sm"></span> <span role="status">Loading...</span>`;
                webshareLoginForm.requestSubmit();
            });
        </script>
    </body>
</html>