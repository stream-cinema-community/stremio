<?php

// If we don't have an authorization code then get one
if (!isset($_GET['code'])) {

    // Fetch the authorization URL from the provider
    $authorizationUrl = $trakt->getAuthorizationUrl();

    // Get the state generated for you and store it to the session.
    $_SESSION['oauth2state'] = $trakt->getState();

    // Redirect the user to the authorization URL.
    redirect($authorizationUrl);

// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || empty($_SESSION['oauth2state']) || $_GET['state'] !== $_SESSION['oauth2state']) {

    if (isset($_SESSION['oauth2state'])) {
        unset($_SESSION['oauth2state']);
    }

    exit('Invalid state');

} else {

    try {
        // Try to get an access token using the authorization code grant.
        $accessToken = $trakt->getAccessToken($_GET['code']);

        // Store access tokens
        $db->save_trakt_tokens($user_id, array(
            'trakt_access_token' => $accessToken->getToken(),
            'trakt_refresh_token' => $accessToken->getRefreshToken(),
            'trakt_expires' => $accessToken->getExpires()
        ));

        $_SESSION['trakt_login_success'] = true;
        redirect('/configure', $_SESSION['configuration_token']);

    } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        exit($e->getMessage());
    }
}
